package com.shenfeld.todo.presentation.viewmodels

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shenfeld.presentation.R
import com.shenfeld.presentation.base.BaseViewModel
import com.shenfeld.todo.domain.models.TaskModel
import com.shenfeld.todo.domain.usecases.InsertTaskInfoUseCase
import com.shenfeld.todo.presentation.fragments.OnSavedTaskListener
import com.shenfeld.todo.presentation.utils.ResourcesManager
import com.shenfeld.weatherioclean.domain.utils.DayUtil
import kotlinx.android.synthetic.main.fragment_add_task.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

class AddTaskViewModel : BaseViewModel() {
    private val insertTaskInfoUseCase: InsertTaskInfoUseCase by inject()

    val completitionLiveData = MutableLiveData<Boolean>()
    val toastLiveData = MutableLiveData<String>()
    private var title: String = ""
    private var description: String = ""

    private fun insertTask() {
        viewModelScope.launch(Dispatchers.IO) {
            val curDate = DayUtil.getSimpleDataForm(dataUnix = Date().time)
            val taskModel = TaskModel(
                title = title,
                description = description,
                currentDate = curDate
            )
            insertTaskInfoUseCase.execute(taskModel)
            completitionLiveData.postValue(true)
        }
    }

    fun titleChanged(newTitle: String) {
        title = newTitle
    }

    fun descriptionChanged(newDescription: String) {
        description = newDescription
    }

    fun insertClicked() {
        if (description.isEmpty() || title.isEmpty()) {
            toastLiveData.postValue(
                resourcesManager.getString(R.string.fill_all_rows)
            )
        } else {
            insertTask()
        }
    }
}