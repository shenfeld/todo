package com.shenfeld.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shenfeld.presentation.base.BaseViewModel
import com.shenfeld.todo.domain.usecases.DeleteAllTaskInfoUseCase
import com.shenfeld.todo.domain.usecases.GetTasksInfoUseCase
import com.shenfeld.todo.presentation.adapters.models.TaskAdapterModel
import com.shenfeld.todo.presentation.utils.ResourcesManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class MainViewModel : BaseViewModel() {
    private val getTasksInfoUseCase: GetTasksInfoUseCase by inject()
    private val deleteAllTaskInfoUseCase: DeleteAllTaskInfoUseCase by inject()

    val tasksLiveData = MutableLiveData<List<TaskAdapterModel>>()

    fun onViewCreated() {
        getTasks()
    }

    private fun getTasks() {
        viewModelScope.launch(Dispatchers.IO) {
            val tasks = getTasksInfoUseCase.execute()
            val tasksModel = tasks.map {
                TaskAdapterModel(
                    title = it.title,
                    description = it.description,
                    currentDate = it.currentDate
                )
            }
            tasksLiveData.postValue(tasksModel)
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            deleteAllTaskInfoUseCase.execute()
            tasksLiveData.postValue(emptyList())
        }
    }
}