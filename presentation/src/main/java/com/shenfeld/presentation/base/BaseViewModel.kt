package com.shenfeld.presentation.base

import androidx.lifecycle.ViewModel
import com.shenfeld.todo.presentation.utils.ResourcesManager
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

abstract class BaseViewModel : ViewModel(), KoinComponent {
    protected val resourcesManager: ResourcesManager by inject()
}