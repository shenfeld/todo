package com.shenfeld.weatherioclean.domain.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object DayUtil {
    private const val DATE_FORMAT = "d MMMM, HH:mm"

    @SuppressLint("SimpleDateFormat")
    fun getSimpleDataForm(dataUnix: Long, dateFormat: String = DATE_FORMAT): String =
        SimpleDateFormat(dateFormat).format(Date(dataUnix))
}

