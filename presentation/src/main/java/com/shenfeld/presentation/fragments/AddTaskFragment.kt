package com.shenfeld.todo.presentation.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.widget.doAfterTextChanged
import com.shenfeld.presentation.R
import com.shenfeld.todo.presentation.viewmodels.AddTaskViewModel
import kotlinx.android.synthetic.main.fragment_add_task.*
import org.koin.androidx.scope.fragmentScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

@Suppress("UNREACHABLE_CODE")
class AddTaskFragment : Fragment() {
    private val viewModel: AddTaskViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupObservers()
    }

    private fun setupObservers() {
        with(viewModel) {
            toastLiveData.observe(viewLifecycleOwner, {
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            })
            completitionLiveData.observe(viewLifecycleOwner, {
                (activity as OnSavedTaskListener?)?.onSavedTask()
                parentFragmentManager.popBackStack()
            })
        }
    }

    private fun setupView() {
        etTitleEdit.doAfterTextChanged {
            viewModel.titleChanged(it.toString())
        }
        etDescriptionEdit.doAfterTextChanged {
            viewModel.descriptionChanged(it.toString())
        }
        ivInsertTask.setOnClickListener {
            viewModel.insertClicked()

        }
    }

    @SuppressLint("ServiceCast")
    @RequiresApi(Build.VERSION_CODES.CUPCAKE)
    override fun onResume() {
        super.onResume()

    }
}

interface OnSavedTaskListener {
    fun onSavedTask()
}