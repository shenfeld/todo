package com.shenfeld.todo.presentation.adapters

import android.os.Build
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.presentation.R
import com.shenfeld.todo.presentation.adapters.models.TaskAdapterModel
import com.shenfeld.todo.presentation.base.BaseAdapter

class TaskAdapter : BaseAdapter<TaskAdapterModel, TaskAdapter.TaskViewHolder>() {
    class TaskViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var tvTaskTitle = v.findViewById<TextView>(R.id.tvTaskTitle)
        internal var tvTaskDescription = v.findViewById<TextView>(R.id.tvTaskDescription)
        internal var tvCurrentDate = v.findViewById<TextView>(R.id.tvCurrentDate)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TaskViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(
                R.layout.rv_item,
                parent,
                false
            )
        return TaskViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        val currentItem = items[position]
        holder.tvTaskTitle.text = currentItem.title
        holder.tvTaskDescription.text = currentItem.description
        holder.tvCurrentDate.text = currentItem.currentDate
    }
}