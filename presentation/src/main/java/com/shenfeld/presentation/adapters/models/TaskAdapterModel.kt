package com.shenfeld.todo.presentation.adapters.models

data class TaskAdapterModel(
    val title: String,
    val description: String,
    val currentDate: String
)