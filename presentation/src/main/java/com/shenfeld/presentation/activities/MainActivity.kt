package com.shenfeld.todo.presentation.activities

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.PopupMenu
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.presentation.R
import com.shenfeld.presentation.adapters.RecyclerItemClickListener
import com.shenfeld.todo.presentation.adapters.TaskAdapter
import com.shenfeld.todo.presentation.fragments.AddTaskFragment
import com.shenfeld.presentation.viewmodels.MainViewModel
import com.shenfeld.todo.presentation.fragments.OnSavedTaskListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.rv_item.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(), OnSavedTaskListener {
    private val viewModel: MainViewModel by viewModel()
    private var adapter: TaskAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
        setupObservers()
        viewModel.onViewCreated()
    }

    private fun setupView() {
        adapter = TaskAdapter()
        rvMain.adapter = adapter
        rvMain.layoutManager = LinearLayoutManager(this)

        ivAddTask.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .replace(R.id.clMainDataContainer, AddTaskFragment())
                .addToBackStack(null)
                .commit()
        }

        ivDeleteAll.setOnClickListener {
            viewModel.deleteAll()
        }

        rvMain.addOnItemTouchListener(
            RecyclerItemClickListener(
                this,
                object : RecyclerItemClickListener.OnItemClickListener {
                    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
                    override fun onItemClick(view: View, position: Int) {
                        ivMoreMenu.setOnClickListener {
                            openOptionMenu(view, position)
                        }
                    }
                })
        )
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun openOptionMenu(view: View, position: Int) {
        val popupMenu = PopupMenu(view.context, view)
        popupMenu.menuInflater.inflate(R.menu.rv_menu_item, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.deleteTask -> println("deleted")
                R.id.editTask -> println("edited")
            }
            true
        }
        popupMenu.show()
    }

    private fun setupObservers() {
        with(viewModel) {
            tasksLiveData.observe(this@MainActivity) {
                adapter?.updateAllItems(it)
            }
        }
    }

    override fun onSavedTask() {
        viewModel.onViewCreated()
    }
}