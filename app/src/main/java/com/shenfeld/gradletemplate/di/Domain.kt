package com.shenfeld.todo.di

import com.shenfeld.todo.domain.usecases.DeleteAllTaskInfoUseCase
import com.shenfeld.todo.domain.usecases.GetTasksInfoUseCase
import com.shenfeld.todo.domain.usecases.InsertTaskInfoUseCase
import com.shenfeld.todo.domain.usecases_impl.DeleteAllTaskInfoUseCaseImpl
import com.shenfeld.todo.domain.usecases_impl.GetTasksInfoUseCaseImpl
import com.shenfeld.todo.domain.usecases_impl.InsertTaskInfoUseCaseImpl
import org.koin.dsl.module

val domainModule = module {
    factory<GetTasksInfoUseCase> {
        GetTasksInfoUseCaseImpl(taskDataSource = get())
    }

    factory<InsertTaskInfoUseCase> {
        InsertTaskInfoUseCaseImpl(taskDataSource = get())
    }

    factory<DeleteAllTaskInfoUseCase> {
        DeleteAllTaskInfoUseCaseImpl(taskDataSource = get())
    }
}