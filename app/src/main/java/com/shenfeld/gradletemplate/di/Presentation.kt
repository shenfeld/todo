package com.shenfeld.todo.di

import com.shenfeld.todo.presentation.utils.ResourcesManager
import com.shenfeld.todo.presentation.viewmodels.AddTaskViewModel
import com.shenfeld.presentation.viewmodels.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    viewModel { MainViewModel() }
    viewModel { AddTaskViewModel() }

    single { ResourcesManager(androidContext()) }

}