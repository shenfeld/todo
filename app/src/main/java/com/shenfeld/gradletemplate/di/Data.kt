package com.shenfeld.todo.di

import androidx.room.Room
import com.shenfeld.todo.data.repositories.TaskRepository
import com.shenfeld.todo.data.room.AppDatabase
import com.shenfeld.todo.domain.repositories.TaskDataSource
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module {
    single<AppDatabase> {
        Room.databaseBuilder(androidContext(), AppDatabase::class.java, "taskDb").build()
    }

    single<TaskDataSource> {
        TaskRepository(appDatabase = get())
    }
}