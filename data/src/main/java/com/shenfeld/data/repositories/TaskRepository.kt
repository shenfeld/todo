package com.shenfeld.todo.data.repositories

import com.shenfeld.todo.data.room.AppDatabase
import com.shenfeld.todo.data.room.entities.Task
import com.shenfeld.todo.domain.models.TaskModel
import com.shenfeld.todo.domain.repositories.TaskDataSource

class TaskRepository(
    private val appDatabase: AppDatabase
) : TaskDataSource {
    override suspend fun getTasks(): List<TaskModel> {
        val tasks = appDatabase.taskDao().getAll()
        return tasks.map {
            TaskModel(
                title = it.titleTask,
                description = it.descriptionTask,
                currentDate = it.dateTask
            )
        }
    }

    override suspend fun insertTask(taskModel: TaskModel) {
        val task = Task(
            titleTask = taskModel.title,
            descriptionTask = taskModel.description,
            dateTask = taskModel.currentDate,
        )
        appDatabase.taskDao().insertAll(task)
    }

    override suspend fun deleteAll() {
        appDatabase.taskDao().deleteAll()
        appDatabase.taskDao().clearPrimaryKey()
    }
}