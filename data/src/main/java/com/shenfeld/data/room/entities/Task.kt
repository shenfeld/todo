package com.shenfeld.todo.data.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Task(
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    @ColumnInfo(name = "title_task") val titleTask: String,
    @ColumnInfo(name = "description_task") val descriptionTask: String,
    @ColumnInfo(name = "date_task") val dateTask: String
)