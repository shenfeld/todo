package com.shenfeld.todo.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.shenfeld.todo.data.room.dao.TaskDao
import com.shenfeld.todo.data.room.entities.Task

@Database(entities = [Task::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun taskDao(): TaskDao
}