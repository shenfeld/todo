package com.shenfeld.todo.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.shenfeld.todo.data.room.entities.Task

@Dao
interface TaskDao {
    @Query("SELECT * FROM task")
    fun getAll(): List<Task>

    @Query("DELETE FROM task")
    fun deleteAll()

    @Query("DELETE FROM sqlite_sequence")
    fun clearPrimaryKey()

    @Insert
    fun insertAll(vararg tasks: Task)

    @Delete
    fun delete(task: Task)
}