package com.shenfeld.todo.domain.models

data class TaskModel(
    val title: String,
    val description: String,
    val currentDate: String
)