package com.shenfeld.todo.domain.usecases_impl

import com.shenfeld.todo.domain.repositories.TaskDataSource
import com.shenfeld.todo.domain.usecases.DeleteAllTaskInfoUseCase

class DeleteAllTaskInfoUseCaseImpl(
    private val taskDataSource: TaskDataSource
) : DeleteAllTaskInfoUseCase {
    override suspend fun execute() {
        taskDataSource.deleteAll()
    }
}