package com.shenfeld.todo.domain.usecases_impl

import com.shenfeld.todo.domain.models.TaskModel
import com.shenfeld.todo.domain.repositories.TaskDataSource
import com.shenfeld.todo.domain.usecases.InsertTaskInfoUseCase

class InsertTaskInfoUseCaseImpl(
    private val taskDataSource: TaskDataSource
) : InsertTaskInfoUseCase {
    override suspend fun execute(taskModel: TaskModel) {
        taskDataSource.insertTask(taskModel)
    }
}