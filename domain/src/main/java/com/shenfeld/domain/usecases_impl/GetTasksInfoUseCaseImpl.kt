package com.shenfeld.todo.domain.usecases_impl

import com.shenfeld.todo.domain.models.TaskModel
import com.shenfeld.todo.domain.repositories.TaskDataSource
import com.shenfeld.todo.domain.usecases.GetTasksInfoUseCase

class GetTasksInfoUseCaseImpl(
    private val taskDataSource: TaskDataSource
) : GetTasksInfoUseCase {
    override suspend fun execute(): List<TaskModel> {
        return taskDataSource.getTasks()
    }
}