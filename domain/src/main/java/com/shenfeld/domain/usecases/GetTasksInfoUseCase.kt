package com.shenfeld.todo.domain.usecases

import com.shenfeld.todo.domain.models.TaskModel

interface GetTasksInfoUseCase {
    suspend fun execute(): List<TaskModel>
}