package com.shenfeld.todo.domain.usecases

import com.shenfeld.todo.domain.models.TaskModel

interface InsertTaskInfoUseCase {
    suspend fun execute(taskModel: TaskModel)
}