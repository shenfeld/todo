package com.shenfeld.todo.domain.usecases

interface DeleteAllTaskInfoUseCase {
    suspend fun execute()
}