package com.shenfeld.todo.domain.repositories

import com.shenfeld.todo.domain.models.TaskModel

interface TaskDataSource {
    suspend fun getTasks(): List<TaskModel>
    suspend fun insertTask(taskModel: TaskModel)
    suspend fun deleteAll()
}